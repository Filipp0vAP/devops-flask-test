FROM python:3.9-alpine

WORKDIR /app

EXPOSE 5000

COPY requirements.txt ./

RUN python -m pip install --upgrade pip && pip install -r ./requirements.txt

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]